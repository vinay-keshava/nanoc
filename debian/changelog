nanoc (4.12.14-2) unstable; urgency=medium

  * debian/ruby-tests.rake: print task names
  * debian/ruby-tests.rake: set CI=1.
    There is at least one test that will try allocating a huge amount of
    memory, causing failures on machines that don't have enough RAM, except
    when CI is set.

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 24 Dec 2022 16:18:57 -0300

nanoc (4.12.14-1) unstable; urgency=medium

  * Team upload

  [ Pirate Praveen ]
  * New upstream version 4.12.7 (upstream switched to terser from uglifier)
  * Drop 0016-filesystem_spec-skip-tests-that-fail-on-debian.patch (merged
    upstream) and refresh other patches (remove fuzz).
  * TODO: need psych 4.0

  [ Antonio Terceiro ]
  * New upstream version 4.12.14
  * Refresh patches
  * Bump Build-Depends: on ruby-contracts to (>= 0.17)
  * Drop build dependencies on ruby-coffee-script
  * debian/ruby-tests.rake: run all test tasks even after one fails
  * debian/ruby-tests.rake: don't run coffee-script test
  * Bump build dependency on ruby-haml to (>= 6.0)
  * debian/rules: ignore test failure under ruby3.0
  * Disable test that checks for the current year (Closes: #1025096)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 23 Dec 2022 11:24:01 -0300

nanoc (4.12.5-4) unstable; urgency=medium

  * Team Upload
  * Switch to terser from uglifier
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Fri, 03 Jun 2022 17:11:41 +0530

nanoc (4.12.5-3) unstable; urgency=medium

  * Add as a suffix the source version to version numbers of binary packages
    with versions different from the source to ensure that the version number
    of these packages will always increase at every upload (Closes: #1004505)
    + revert in particular the previous addition of a + for ruby-nanoc-live

 -- Cédric Boutillier <boutil@debian.org>  Sun, 30 Jan 2022 00:10:57 +0100

nanoc (4.12.5-2) unstable; urgency=medium

  * dh_gencontrol: add + in the fake version number for ruby-nanoc-live
    as the previous package had a gem beta version number which for Debian
    was higher than 1.0
  * Update years in copyright file
  * Remove trailing space in 4.11.23-2 changelog entry
  * Bump Standards-Version to 4.6.0 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Sat, 29 Jan 2022 16:06:24 +0100

nanoc (4.12.5-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * New upstream version 4.12.4

  [ Sergio Durigan Junior ]
  * New upstream version 4.12.5
  * d/control: B-D on ruby-memo-wise.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Thu, 27 Jan 2022 13:52:19 -0500

nanoc (4.11.23-2) unstable; urgency=medium

  * Build-depend on colored (Closes: #997336)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 26 Oct 2021 12:03:09 +0200

nanoc (4.11.23-1) unstable; urgency=medium

  * New upstream version 4.11.23
  * Refresh patches
  * Add 2 patches to adjust the test suite for Debian
  * Add myself to Uploaders:

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 06 Feb 2021 11:28:54 -0300

nanoc (4.11.18-1) unstable; urgency=medium

  * Team upload
  * Add missing ${ruby:Depends} to all binary packages
  * autopkgtest: add smoke test
  * debian/ruby-tests.rake: run tests against installed code

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 01 Feb 2021 09:45:59 -0300

nanoc (4.11.18-1~1) experimental; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * New upstream version 4.11.18
  * New binary package for the external filter

  [ Antonio Terceiro ]
  * Add binary packages for nanoc-checking and nanoc-deploying
  * Refresh patches
  * debian/ruby-tests.rake: Don't Repeat Yourself
    - run tests for new sub-packages
    - do the right thing under autopkgtest
  * debian/rules: produce version numbers matching the gemspecs
  * add new build dependency: ruby-webmock

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 29 Jan 2021 10:17:00 -0300

nanoc (4.11.14-4) unstable; urgency=medium

  * Skip nanoc-live tests failing on ipv6-only builders (Closes: #972701)
  * Use the correct json shema gem as dependency for ruby-nanoc-core

 -- Cédric Boutillier <boutil@debian.org>  Wed, 18 Nov 2020 14:26:19 +0100

nanoc (4.11.14-3) unstable; urgency=medium

  * Use asciidoc instead of asciidoc-base package name in dependencies
    (Closes: #971653)
  * Remove or reshuffle some dependencies between packages
  * Rules doesn't require root

 -- Cédric Boutillier <boutil@debian.org>  Tue, 13 Oct 2020 15:40:03 +0200

nanoc (4.11.14-2) unstable; urgency=medium

  * Make nanoc depend on ruby-nanoc-cli (Closes: #968056)
  * Bump debhelper compatibility level to 13
  * Use secure URI in Homepage field.

 -- Cédric Boutillier <boutil@debian.org>  Wed, 26 Aug 2020 16:21:09 +0200

nanoc (4.11.14-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Cédric Boutillier ]
  * New upstream version 4.11.14
  * Use gem installation layout
  * Refresh patches
  * Create new ruby-nanoc-core and ruby-nanoc-cli binary packages
  * Drop nanoc-doc package
  * Skip failing tests
  * Add upstream/metadata and .gitattributes
  * Add README.md as doc for nanoc package
  * Remove symlink for gemspec in debian/
  * remove old NEWS entry
  * add tests for nanoc-live
  * debian/patches:
    + skip gem and manifest generation specs for nanoc-live
    + Force external encoding to UTF-8 for live_recompiler_spec
    + Add live command if ruby-nanoc-live installed

  [ Marc Dequènes (Duck) ]
  * add ruby-nanoc-live
  * Add missing Ruby interpreter dependency.
  * Enable dependency checking

 -- Cédric Boutillier <boutil@debian.org>  Mon, 27 Jul 2020 12:16:36 +0200

nanoc (4.11.0-5) unstable; urgency=medium

  * Replace python-pygments by python3-pygments to ease removal of Python 2
    from bullseye. Thanks Sandro Tosi. (Closes: #943191)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 07 Apr 2020 01:53:43 +0200

nanoc (4.11.0-4) unstable; urgency=medium

  * Fix issues with tests (Closes: #952038)
    + Add patch to skip tests failing with newer ruby-rouge
    + Add requirement on ruby-colored, dropped from newer ruby-cri
    + Skip patches relying on SAFE mechanism, dropped in ruby2.7
    + Skip checksum spec issues with ruby2.7

 -- Cédric Boutillier <boutil@debian.org>  Thu, 12 Mar 2020 14:14:23 +0100

nanoc (4.11.0-3) unstable; urgency=medium

  * Change (build-)dependencies from ruby-fog (removed from the archive) to
    ruby-fog-local (Closes: #924845)

 -- Cédric Boutillier <boutil@debian.org>  Thu, 12 Mar 2020 14:13:40 +0100

nanoc (4.11.0-2) unstable; urgency=medium

  * Tighten ruby-hamster and ruby-ref dependency relation from Recommends to
    Depends. (Closes: #916724)
  * Add minimal version 1.3 to ruby-adsf dependency (Closes: #917207)
  * Change dependency from python-pygments to python3-pygments to find the
    pygmentize script (Closes: #917890)
  * Link nanoc gemspec under debian/ to make dependencies checkable in
    autopkgtest (Closes: #917144)

 -- Cédric Boutillier <boutil@debian.org>  Sun, 06 Jan 2019 22:10:41 +0100

nanoc (4.11.0-1) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.2.1 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Sat, 22 Dec 2018 11:55:13 +0100

nanoc (4.11.0-1~exp1) experimental; urgency=medium

  * New upstream version 4.11.0
  * Use Github as the source to get tests
  * Change to multibinary layout, although we only build nanoc so far
    + adapt paths for patches and rules to find tests to run
  * Add dependencies on ddmetrics ddmemoize and slow_enumerator_tools,
  * (build)depend on ruby-brandur-json-schema (json_schema gem), ruby-erubi,
    ruby-parallel, ruby-tomlrb
  * Refresh no_privacy_breach patch and filter_out_nil_gemspec.patch
  * New rspec_* patches: skip some failing rspec
  * Use https in copyright format URL
  * Disable gemspec dependencies for now

 -- Cédric Boutillier <boutil@debian.org>  Mon, 10 Dec 2018 23:38:05 +0100

nanoc (4.8.10-2) unstable; urgency=medium

  * Skip temporarily cli failing test (Closes: #892195)
  * Skip failing autopkgtest with local firewall (Closes: #893447).
    Thanks Steve Langasek for the patch.
  * Sort files when producing the documentation (Closes: #884936).
    Thanks Chris Lamb for the patch.
  * Move debian/watch to gemwatch.debian.net
  * Bump Standards-Version to 4.1.5 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Mon, 30 Jul 2018 17:18:00 +0200

nanoc (4.8.10-1) unstable; urgency=medium

  * New upstream version 4.8.10
  * Refresh filter_out_nil_gemspec.patch
  * Bump Standards-Version to 4.1.1 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 17 Oct 2017 21:49:22 +0200

nanoc (4.8.9-1) unstable; urgency=medium

  * New upstream version 4.8.9
    + add new dependency on ruby-addressable

 -- Cédric Boutillier <boutil@debian.org>  Mon, 25 Sep 2017 14:59:37 +0200

nanoc (4.8.5-1) unstable; urgency=medium

  * New upstream version 4.8.5
  * Remove dependency on (broken) ruby-compass (Closes: #875510)
  * Bump Standards-Version to 4.1.0

 -- Cédric Boutillier <boutil@debian.org>  Tue, 12 Sep 2017 15:51:08 +0200

nanoc (4.8.0-1) unstable; urgency=medium

  * New upstream version 4.8.0
    + Adds support for asciidoctor filter
  * Bump Debhelper compatibility version to 10
  * Refresh skip_tests_autopkgtest.patch

 -- Cédric Boutillier <boutil@debian.org>  Thu, 20 Jul 2017 17:08:05 +0200

nanoc (4.7.13-1) unstable; urgency=medium

  * New upstream version 4.7.13
  * Upload to unstable
  * refresh patches
  * Build-depend on ruby-fuubar
  * Bump Standards-version to 4.0.0 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Sun, 02 Jul 2017 11:45:10 +0200

nanoc (4.7.9-1~exp1) experimental; urgency=medium

  * New upstream version 4.7.9

  [Antonio Terceiro]
  * debian/rules: export TZ=UTC to make sure the tests pass regardless of the
    local timezone

 -- Cédric Boutillier <boutil@debian.org>  Thu, 04 May 2017 11:01:36 +0200

nanoc (4.7.8-1~exp1) experimental; urgency=medium

  * New upstream version 4.7.8
  * Refresh no_privacy_breach.patch, no_simplecov.patch, no_vcr.patch,
    no_w3c_validators.patch
  * Bump version in ruby-cri dependency
  * Depend on ruby-nokogumbo and ruby-ddplugin
  * Build-depend and suggest git

 -- Cédric Boutillier <boutil@debian.org>  Sat, 22 Apr 2017 09:08:37 +0200

nanoc (4.4.7-3) unstable; urgency=medium

  * Team upload.
  * debian/rules: export TZ=UTC to make sure the tests pass regardless of the
    local timezone (Closes: #861119)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 27 Apr 2017 19:09:59 -0300

nanoc (4.4.7-2) unstable; urgency=medium

  * Depend on asciidoc-base instead of asciidoc (Closes: #850357)
  * Mark nanoc-doc as Multi-Arch: foreign
  * Update manpage
  * Enable dependency checks in debian/rules

 -- Cédric Boutillier <boutil@debian.org>  Tue, 10 Jan 2017 14:47:00 +0100

nanoc (4.4.7-1) unstable; urgency=medium

  * New upstream version 4.4.7
  * Capitalize Nanoc where possible, as upstream official name
  * Rely on gem2deb rake tasks to run tests in ruby-tests.rake
  * Run now also tests in spec/ together with those in test/
  * Exclude tests on 'less' gem (until it gets packaged)
  * Move old rake task to debian/ and generate documentation with it

 -- Cédric Boutillier <boutil@debian.org>  Wed, 28 Dec 2016 23:03:49 +0100

nanoc (4.3.4-1) unstable; urgency=medium

  * New upstream version 4.3.4

 -- Cédric Boutillier <boutil@debian.org>  Thu, 06 Oct 2016 22:02:43 +0200

nanoc (4.3.2-1) unstable; urgency=medium

  * Imported Upstream version 4.3.2

 -- Cédric Boutillier <boutil@debian.org>  Tue, 23 Aug 2016 21:48:40 +0200

nanoc (4.2.4-1) unstable; urgency=medium

  * Imported Upstream version 4.2.4
  * Depend on ruby-ref

 -- Cédric Boutillier <boutil@debian.org>  Mon, 01 Aug 2016 23:18:28 +0200

nanoc (4.2.3-1) unstable; urgency=medium

  * Imported Upstream version 4.2.3
  * Depend on ruby-rouge >= 2 now it is in the archive
    and drop test modification for older version

 -- Cédric Boutillier <boutil@debian.org>  Sun, 03 Jul 2016 18:50:55 +0200

nanoc (4.2.2-1) unstable; urgency=medium

  * Imported Upstream version 4.2.2
  * Build-depend on ruby-contracts and ruby-hamster
  * Adapt tests to (now old) ruby-rouge 1.x

 -- Cédric Boutillier <boutil@debian.org>  Sat, 02 Jul 2016 21:49:06 +0200

nanoc (4.2.0-1) unstable; urgency=medium

  * Imported Upstream version 4.2.0
  * Depend on ruby-adsf (Closes: #819357)
  * Bump Standards-version to 3.9.8 (no changes needed)
  * Refresh deactivate_test_huge_site.patch
  * Add skip_tests_autopkgtest.patch to improve autopkgtest results

 -- Cédric Boutillier <boutil@debian.org>  Mon, 06 Jun 2016 15:20:38 +0200

nanoc (4.1.4-2) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files

  [ Christian Hofstaedtler ]
  * Drop ruby-bluecloth dependency. Please use rdiscount instead.

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 01 May 2016 20:09:44 +0000

nanoc (4.1.4-1) unstable; urgency=medium

  * Upload to unstable
  * Imported Upstream version 4.1.4
  * Use the Rake method to run the tests
  * Add patch to filter out nil Gem::Specifications with Ruby 2.3
    (fixes tests for ruby2.3)
  * Add encoding patch to fix test involving non ASCII chars in clean
    environment

 -- Cédric Boutillier <boutil@debian.org>  Mon, 29 Feb 2016 13:03:12 +0100

nanoc (4.1.2-1~exp1) experimental; urgency=medium

  * Imported Upstream version 4.1.2
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Use https to fix insecure Vcs-* fields
  * Use debhelper compatibility level 9
  * Unapply deactivate_rubygems_in_tests.patch and
    no_bundler_no_loadpath_modif.patch, not needed anymore
  * Add news with a link to upstream upgrade guide
  * Move the the web section of the archive

 -- Cédric Boutillier <boutil@debian.org>  Sat, 20 Feb 2016 22:08:51 +0100

nanoc (3.8.0-1) unstable; urgency=medium

  * Imported Upstream version 3.8.0
  * Refresh patches
    + drop deactivate_uglify_test.patch (not needed anymore)
    + add set-load_paths-for-sass-tests.patch because of a change in default
      options of ruby-sass 3.4.14
  * Depend (but do not build-depend) on ruby-rouge
      tests using rouge are failing for the moment. Not adding it to
      build-depends deactivates these tests.
  * Remove coverage/ when cleaning

 -- Cédric Boutillier <boutil@debian.org>  Sat, 20 Jun 2015 23:57:13 +0200

nanoc (3.7.3-1) unstable; urgency=medium

  * Imported Upstream version 3.7.3
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Set Testsuite field to autopkgtest-pkg-ruby

 -- Cédric Boutillier <boutil@debian.org>  Mon, 06 Oct 2014 23:40:01 +0200

nanoc (3.7.2-1) unstable; urgency=medium

  * Imported Upstream version 3.7.2
  * Refresh patches

 -- Cédric Boutillier <boutil@debian.org>  Thu, 21 Aug 2014 16:17:45 +0200

nanoc (3.7.0-1) unstable; urgency=medium

  * Imported Upstream version 3.7.0
  * Refresh no_bundler_no_loadpath_modif.patch and no_vcr.patch
  * Build-depend on ruby-coveralls

 -- Cédric Boutillier <boutil@debian.org>  Mon, 09 Jun 2014 23:38:41 +0200

nanoc (3.6.11-1) unstable; urgency=medium

  * Imported Upstream version 3.6.11
  * debian/patches:
    + remove unapplied add_if_have.patch
    + add no_vcr.patch waiting for ruby-vcr to be packaged
    + refresh deactivate_test_huge_site.patch and
      deactivate_rubygems_in_tests.patch

 -- Cédric Boutillier <boutil@debian.org>  Fri, 09 May 2014 17:24:21 +0200

nanoc (3.6.9-1) unstable; urgency=medium

  * Imported Upstream version 3.6.9
  * Remove identifier.rb from debian/ since it is now available in the source
  * Refresh no_privacy_breach.patch and no_rdoc4.patch
  * Dependencies:
    - add highlight and ruby-pygments.rb as suggestions
    - drop dependency on ruby-systemu

 -- Cédric Boutillier <boutil@debian.org>  Tue, 15 Apr 2014 11:13:08 +0200

nanoc (3.6.7-1) unstable; urgency=medium

  * Initial release (Closes: #714849)

 -- Cédric Boutillier <boutil@debian.org>  Fri, 14 Feb 2014 12:18:18 +0100
